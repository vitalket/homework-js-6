/* 
   1. Екранування потрібно для зміни поведінки спец символів, наприклад лапки "", для того щоб їх вивести в строку
      потрібно використовувати \

   2. Функция декларейшн function getSum() {}, функція екпрешн const getSum = function() {},
      також стрілкова функція і методи об'єкта, 

   3. Hoisting - це механізм в JS, який дозволяє змінним та функціям бути доступними для використання навіть перед тим, 
      як вони були оголошені у коді. 
 */



function createNewUser() {
   const newUser = {};

   newUser.firstName = prompt("Ваше ім'я?");
   newUser.lastName = prompt("Ваше прізвище?");
   newUser.birthday = prompt("Введіть дату народження у форматі dd.mm.yyyy");

   newUser.getLogin = function () {
      let str = this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
      return str;
   }

   newUser.getAge = function () {
      const currentDate = new Date();
      const birthDate = new Date(this.birthday.split(/[.,]/).reverse().join("-"));
      
      let userAge = currentDate.getFullYear() - birthDate.getFullYear();
      const monthDiff = currentDate.getMonth() - birthDate.getMonth();
      if (monthDiff < 0 || (monthDiff === 0 && currentDate.getDate() < birthDate.getDate())) {
         userAge--;
      }
      return userAge;;
   }

   newUser.getPassword = function () {
      const firstLetter = this.firstName.charAt(0).toUpperCase();
      const lastNameLower = this.lastName.toLowerCase();
      const birthYear = this.birthday.split(/[.,]/)[2];
      const password = firstLetter + lastNameLower + birthYear;
      return password;
   }

   return newUser;

}

const user = createNewUser();
console.log("User:", user);

const login = user.getLogin();
console.log("Логін:", login);

const age = user.getAge();
console.log("Ваш вік:", age);

const password = user.getPassword();
console.log("Пароль:", password);

























